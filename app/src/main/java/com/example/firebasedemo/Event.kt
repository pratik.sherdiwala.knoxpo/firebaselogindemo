package com.example.firebasedemo

open class Event<out T>(private val content: T) {

    var hasBeenhandled = false
        private set

    fun getContentIfNothandled(): T? {
        return if (hasBeenhandled) {
            null
        } else {
            hasBeenhandled = true
            content
        }
    }

    fun peekContent(): T = content
}