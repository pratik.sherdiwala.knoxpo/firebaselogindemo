package com.example.firebasedemo.data

import android.util.Log
import com.example.firebasedemo.extentions.toSingle
import com.example.firebasedemo.model.User
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import io.reactivex.Single
import javax.inject.Inject

private val TAG = UserRepository::class.java.simpleName

class UserRepository @Inject constructor() {

    private val auth = FirebaseAuth.getInstance()


    fun signUp(user: User) {
        val db = FirebaseFirestore.getInstance()
        val dbUsers = db.collection("users")

        val storingUser = hashMapOf(
            "address" to user.address,
            "contact" to user.contact,
            "email" to user.email,
            "name" to user.name,
            "password" to user.password
        )

        dbUsers.add(user)
            .addOnSuccessListener {
                Log.d(TAG, "User Added")
            }
            .addOnFailureListener {
                Log.e(TAG, "$it")
            }

        auth.createUserWithEmailAndPassword(user.email!!, user.password!!)
            .addOnCompleteListener {
                if (it.isSuccessful) {
                    Log.d(TAG, "User Created")
                } else {
                    Log.e(TAG, "User Cant be created")
                }
            }
    }

    fun signIn(username: String, password: String) {
        auth.signInWithEmailAndPassword(username, password)
            .addOnCompleteListener {
                if (it.isSuccessful) {
                    Log.d(TAG, "Login SuccessFul")
                } else {
                    Log.d(TAG, "Login Failed")
                }
            }.addOnFailureListener {

            }
    }

    fun getAllUsers(): Single<MutableList<User>> {

        val userList = mutableListOf<User>()

        val db = FirebaseFirestore.getInstance()
        return db.collection("users")
            .get()
            .toSingle()
            .map {
                val documentList = it.documents
                for (document in documentList) {
                    //val user = document.toObject(User::class.java)
                    userList.add(User(document))
                    Log.d(TAG, "$userList")
                }
                userList
            }

    }
}