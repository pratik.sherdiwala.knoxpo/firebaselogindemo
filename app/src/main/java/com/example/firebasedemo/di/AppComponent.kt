package com.example.firebasedemo.di

import com.example.firebasedemo.App
import com.example.firebasedemo.di.module.LoginActivityModule
import com.example.firebasedemo.di.module.SignUpActivityModule
import com.example.firebasedemo.di.module.UserListActivityModule
import com.example.firebasedemo.di.module.ViewModelModule
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidInjectionModule::class,
        AndroidSupportInjectionModule::class,
        LoginActivityModule::class,
        ViewModelModule::class,
        SignUpActivityModule::class,
        UserListActivityModule::class
    ]
)

interface AppComponent {

    fun inject(app: App)

}