package com.example.firebasedemo.di.module

import com.example.firebasedemo.ui.signup.SignUpActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class SignUpActivityModule {

    @ContributesAndroidInjector
    abstract fun contributeSignUpActivity(): SignUpActivity

}