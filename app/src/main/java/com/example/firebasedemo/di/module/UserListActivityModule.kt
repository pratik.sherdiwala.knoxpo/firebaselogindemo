package com.example.firebasedemo.di.module

import com.example.firebasedemo.ui.userlist.UserListActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class UserListActivityModule {

    @ContributesAndroidInjector
    abstract fun contributeUserListViewModel(): UserListActivity

}