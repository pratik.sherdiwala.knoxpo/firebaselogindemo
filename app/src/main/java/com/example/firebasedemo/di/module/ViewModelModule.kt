package com.example.firebasedemo.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.firebasedemo.ui.login.LoginViewModel
import com.example.firebasedemo.ui.signup.SignUpViewModel
import com.example.firebasedemo.ui.userlist.UserListViewModel
import com.example.firebasedemo.viewmodel.AppViewModelFactory
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(LoginViewModel::class)
    abstract fun bindLoginViewModel(viewModel: LoginViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SignUpViewModel::class)
    abstract fun bindSignUpViewModel(viewModel: SignUpViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(UserListViewModel::class)
    abstract fun bindUserListViewModel(viewModel: UserListViewModel): ViewModel


    @Binds
    abstract fun provideAppviewModelFactory(appViewModelFactory: AppViewModelFactory): ViewModelProvider.Factory

}