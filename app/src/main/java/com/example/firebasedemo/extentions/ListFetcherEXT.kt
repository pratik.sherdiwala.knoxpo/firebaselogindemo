package com.example.firebasedemo.extentions

import com.google.android.gms.tasks.Task
import io.reactivex.Single

inline fun <reified T> Task<T>.toSingle(): Single<T> {
    return Single.create { singleEmmiter ->
        this
            .addOnSuccessListener {
                singleEmmiter.onSuccess(it)
            }
            .addOnFailureListener {
                singleEmmiter.onError(it)
            }
    }
}