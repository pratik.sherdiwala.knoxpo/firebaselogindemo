package com.example.firebasedemo.model

import com.google.firebase.firestore.DocumentSnapshot

class User {

    var name: String? = null
    var contact: String? = null
    var address: String? = null
    var email: String? = null
    var password: String? = null

    constructor(documentSnapshot: DocumentSnapshot) {
        name = documentSnapshot["name"] as String
        address = documentSnapshot["address"] as String
        email = documentSnapshot["email"] as String
        contact = documentSnapshot["contact"] as String
        password = documentSnapshot["password"] as String
    }

    constructor(){

    }
}

