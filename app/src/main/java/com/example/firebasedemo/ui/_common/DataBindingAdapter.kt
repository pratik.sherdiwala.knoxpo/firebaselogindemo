package com.example.firebasedemo.ui._common

import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.firebasedemo.model.User
import com.example.firebasedemo.ui.userlist.adapter.UserListAdapter

object DataBindingAdapter {

    @JvmStatic
    @BindingAdapter("user")
    fun setUser(view: RecyclerView, users: List<User>?) {
        users?.let {
            (view.adapter as UserListAdapter).updateUser(it)
        }
    }
}