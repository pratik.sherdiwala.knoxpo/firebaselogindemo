package com.example.firebasedemo.ui.login

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.example.firebasedemo.R
import com.example.firebasedemo.ui._common.DataBindingActivity
import com.example.firebasedemo.ui.signup.SignUpActivity
import com.example.firebasedemo.ui.userlist.UserListActivity
import dagger.android.AndroidInjection
import javax.inject.Inject

class LoginActivity : DataBindingActivity<com.example.firebasedemo.databinding.ActivityLoginBinding>(),
    Navigation {

    override val layoutId = R.layout.activity_login

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel by lazy {
        ViewModelProviders.of(
            this,
            viewModelFactory
        ).get(
            LoginViewModel::class.java
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)

        val activity = this

        with(binding) {
            lifecycleOwner = activity
            viewModel = activity.viewModel
        }

        with(viewModel) {
            showSignUpEvent.observe(activity, Observer {
                it?.getContentIfNothandled()?.let {
                    navigateSignUp()
                }
            })

            loginSuccessEvent.observe(activity, Observer {
                it.getContentIfNothandled()?.let {
                    Toast.makeText(activity, "Login SuccessFul", Toast.LENGTH_SHORT).show()
                    navigateDetail()
                }
            })

            loginFailureEvent.observe(activity, Observer {
                it.getContentIfNothandled()?.let {
                    Toast.makeText(activity, "Login Fail", Toast.LENGTH_SHORT).show()
                }
            })
        }
    }

    override fun navigateSignUp() {
        startActivity(Intent(this, SignUpActivity::class.java))
    }

    override fun navigateDetail() {
        startActivity(Intent(this, UserListActivity::class.java))
    }
}