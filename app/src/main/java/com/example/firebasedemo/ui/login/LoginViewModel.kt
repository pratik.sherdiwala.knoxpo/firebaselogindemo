package com.example.firebasedemo.ui.login

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.firebasedemo.Event
import com.example.firebasedemo.data.UserRepository
import javax.inject.Inject

class LoginViewModel @Inject constructor(
    private val userRepository: UserRepository
) : ViewModel() {

    val username = MutableLiveData<String>()
    val password = MutableLiveData<String>()

    private val _showSignUpEvent = MutableLiveData<Event<Unit>>()
    val showSignUpEvent: LiveData<Event<Unit>>
        get() = _showSignUpEvent

    private val _loginSuccessEvent = MutableLiveData<Event<Unit>>()
    val loginSuccessEvent: LiveData<Event<Unit>>
        get() = _loginSuccessEvent

    private val _loginFailureEvent = MutableLiveData<Event<Unit>>()
    val loginFailureEvent: LiveData<Event<Unit>>
        get() = _loginFailureEvent

    fun showSignUp() {
        _showSignUpEvent.value = Event(Unit)
    }

    fun signIn() {
        if (username.value != null && password.value != null) {
            userRepository.signIn(username.value!!, password.value!!)
            _loginSuccessEvent.value = Event(Unit)
        } else {
            _loginFailureEvent.value = Event(Unit)
        }
    }
}