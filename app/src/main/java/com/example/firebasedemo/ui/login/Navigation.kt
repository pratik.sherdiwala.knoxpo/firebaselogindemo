package com.example.firebasedemo.ui.login

interface Navigation {

    fun navigateSignUp()

    fun navigateDetail()

}