package com.example.firebasedemo.ui.signup

import android.os.Bundle
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.example.firebasedemo.R
import com.example.firebasedemo.ui._common.DataBindingActivity
import dagger.android.AndroidInjection
import javax.inject.Inject

private val TAG = SignUpActivity::class.java.simpleName

class SignUpActivity : DataBindingActivity<com.example.firebasedemo.databinding.ActivitySignUpBinding>() {

    override val layoutId = R.layout.activity_sign_up

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel by lazy {
        ViewModelProviders.of(
            this,
            viewModelFactory
        ).get(SignUpViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)

        val activity = this

        with(binding) {
            lifecycleOwner = activity
            viewModel = activity.viewModel
        }

        with(viewModel) {

        }
    }
}