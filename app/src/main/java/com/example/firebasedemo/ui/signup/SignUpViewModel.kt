package com.example.firebasedemo.ui.signup

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.firebasedemo.Event
import com.example.firebasedemo.data.UserRepository
import com.example.firebasedemo.model.User
import javax.inject.Inject

class SignUpViewModel @Inject constructor(
    private val userRepository: UserRepository
) : ViewModel() {

    val name = MutableLiveData<String>()
    val contact = MutableLiveData<String>()
    val address = MutableLiveData<String>()
    val email = MutableLiveData<String>()
    val password = MutableLiveData<String>()

    private val _signUpSuccessEvent = MutableLiveData<Event<Unit>>()
    val signUpSuccessEvent: LiveData<Event<Unit>>
        get() = _signUpSuccessEvent

    private val _signUpFailureEvent = MutableLiveData<Event<Unit>>()
    val signUpFailureEvent: LiveData<Event<Unit>>
        get() = _signUpFailureEvent

    fun signUp() {
        if (name.value != null || contact.value != null || address.value != null || email.value != null || password.value != null) {
            val user = User()
            user.name = name.value!!
            user.contact = contact.value!!
            user.address = address.value!!
            user.email = email.value!!
            user.password = password.value!!
            userRepository.signUp(user)
            _signUpSuccessEvent.value = Event(Unit)
        } else {
            _signUpFailureEvent.value = Event(Unit)
        }
    }
}