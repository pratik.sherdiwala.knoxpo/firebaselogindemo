package com.example.firebasedemo.ui.userlist

import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.firebasedemo.R
import com.example.firebasedemo.databinding.ActivityUserListBinding
import com.example.firebasedemo.ui._common.DataBindingActivity
import com.example.firebasedemo.ui.userlist.adapter.UserListAdapter
import dagger.android.AndroidInjection
import javax.inject.Inject

class UserListActivity : DataBindingActivity<ActivityUserListBinding>() {

    override val layoutId = R.layout.activity_user_list

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel by lazy {
        ViewModelProviders.of(
            this,
            viewModelFactory
        ).get(
            UserListViewModel::class.java
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)

        val activity = this

        with(binding) {
            lifecycleOwner = activity
            viewModel = activity.viewModel

            userRV.layoutManager = LinearLayoutManager(activity)
            userRV.adapter = UserListAdapter()
        }

        with(viewModel) {
            fetchUsers()
        }
    }
}