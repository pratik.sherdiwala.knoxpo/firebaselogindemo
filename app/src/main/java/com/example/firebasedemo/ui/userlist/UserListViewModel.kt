package com.example.firebasedemo.ui.userlist

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.firebasedemo.data.UserRepository
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class UserListViewModel @Inject constructor(
    private val userRepository: UserRepository
) : ViewModel() {

    private val TAG = UserListViewModel::class.java.simpleName

    var userList = MutableLiveData<List<com.example.firebasedemo.model.User>>()

    private val disposables = CompositeDisposable()

    fun fetchUsers() {
        userRepository.getAllUsers().subscribe(
            {
                userList.value = it
                Log.d(TAG, it.size.toString())
            },
            {

            }
        ).also {
            disposables.add(it)
        }
    }

    override fun onCleared() {
        super.onCleared()
        disposables.dispose()
    }
}