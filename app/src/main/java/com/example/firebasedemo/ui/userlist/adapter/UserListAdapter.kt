package com.example.firebasedemo.ui.userlist.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.firebasedemo.R
import com.example.firebasedemo.model.User
import com.example.firebasedemo.ui.userlist.adapter.viewholder.UserVH

class UserListAdapter : RecyclerView.Adapter<UserVH>() {

    var userList: List<User>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserVH {
        return UserVH(
            LayoutInflater.from(parent.context)
                .inflate(
                    R.layout.item_user,
                    parent,
                    false
                )
        )
    }

    override fun getItemCount() = userList?.size ?: 0

    override fun onBindViewHolder(holder: UserVH, position: Int) {
        holder.bindUser(userList!![position])
    }

    fun updateUser(newUser: List<User>) {
        userList = newUser
        notifyDataSetChanged()
    }
}