package com.example.firebasedemo.ui.userlist.adapter.viewholder

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.firebasedemo.R
import com.example.firebasedemo.model.User

class UserVH(itemView: View) : RecyclerView.ViewHolder(itemView) {

    private val mNameTV = itemView.findViewById<TextView>(R.id.nameTV)
    private val mAddressTV = itemView.findViewById<TextView>(R.id.addressTV)
    private val mContactTV = itemView.findViewById<TextView>(R.id.contactTV)

    fun bindUser(user: User) {
        mNameTV.text = user.name
        mAddressTV.text = user.address
        mContactTV.text = user.contact
    }
}